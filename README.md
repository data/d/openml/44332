# OpenML dataset: Meta_Album_MED_LF_Extended

https://www.openml.org/d/44332

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Medicinal Leaf Dataset (Extended)**
***
The Medicinal Leaf Database(https://data.mendeley.com/datasets/nnytj2v3n5/1) gathers 30 species of healthy and mature medicinal herbs. The leaves are plucked from different plants of the same species, then placed on a white uniform background. There are around 1 800 images in total, captured with a mobile phone camera. The original resolution is 1 600x1 200 px. We create Medleaf for Meta-Album by cropping them at the center and resize to 128x128 px.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/MED_LF.png)

**Meta Album ID**: PLT_DIS.MED_LF  
**Meta Album URL**: [https://meta-album.github.io/datasets/MED_LF.html](https://meta-album.github.io/datasets/MED_LF.html)  
**Domain ID**: PLT_DIS  
**Domain Name**: Plant Diseases  
**Dataset ID**: MED_LF  
**Dataset Name**: Medicinal Leaf  
**Short Description**: Healthy Medicinal Leaf  
**\# Classes**: 26  
**\# Images**: 1596  
**Keywords**: medicinal leaf, plants, plant diseases  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: CC BY 4.0  
**License URL(original data release)**: https://data.mendeley.com/datasets/nnytj2v3n5/1
https://creativecommons.org/licenses/by/4.0/
 
**License (Meta-Album data release)**: CC BY 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)  

**Source**: Medicinal Leaf Dataset  
**Source URL**: https://data.mendeley.com/datasets/nnytj2v3n5/1  
  
**Original Author**: S Roopashree, J Anitha  
**Original contact**:   

**Meta Album author**: Phan Anh VU  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
 @article{s_j_2020, 
     title={Medicinal Leaf Dataset}, 
     url={https://data.mendeley.com/datasets/nnytj2v3n5/1}, 
     author={S, Roopashree and J, Anitha}, 
     year={2020}, 
     month={Oct},
     doi={10.17632/nnytj2v3n5.1},
     version={1},
     publisher={Mendeley Data}   
} 
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44314)  [[Mini]](https://www.openml.org/d/44299)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44332) of an [OpenML dataset](https://www.openml.org/d/44332). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44332/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44332/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44332/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

